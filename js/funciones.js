var url = new URLSearchParams(location.search);
var url_api = "https://madarme.gitlab.io/persistencia_parcial/preguntas.json";
var preguntas;
var puntaje = 0;

function loadRespuestas(){
    let answer = "answer##";
    let cantidad = url.get("cantidad");
    let nombre = url.get("nombre");
    let respuesta;
    document.getElementById("nombre").innerHTML = "Hola " + nombre + ", a continuación tiene el resultado de su evaluacion:";

    fetch(url_api).then(response => response.json()).then(response => {
        preguntas = response;
        let content = "";
        for(let i=0; i<cantidad; i++){
            respuesta = url.get(answer.replace("##", i));
            content += getRow(preguntas[i], respuesta, i+1);
        }
        document.getElementById("table").innerHTML = content;
        document.getElementById("puntaje").innerHTML = "Puntaje: "+ ((puntaje * 100) / cantidad) + "%";
    });
}

function getRow(pregunta, respuesta, n){
    let tr = "<tr>@@</tr>";
    let td = "<td class='##'>@@</td>";
    let content = td.replace("@@", n);
    for(let i=0; i<2; i++){
        if(pregunta.OPCION_VALIDA == respuesta){
            content += td.replace("##", "bg-success").replace("@@", "x");
            puntaje ++; 
        }
        else {
            content += td.replace("@@", " ") + td.replace("##", "bg-danger").replace("@@", "x");
        }
        i++;
    }
    return tr.replace("@@", content);
}

function loadPreguntas() {
    let nPreguntas = url.get("cantidad");
    let nombre = "<input type='hidden' value='@@' name='nombre'>".replace("@@", url.get("nombre"));
    let cantidad = "<input type='hidden' value='@@' name='cantidad'>".replace("@@", nPreguntas)

    fetch(url_api).then(response => response.json()).then(response => {
        preguntas = response;
        let content = "";
        for(let i=0; i<nPreguntas; i++){
            content += getPregunta(preguntas[i], i);
        }
        content += nombre + cantidad + "<input class='btn btn-secondary' type='submit'>";
        document.getElementById("form").innerHTML = content;
    });
}

function getPregunta(content, n) {
    let div = "<div>@@</div>";
    let pregunta =
        "<div class='row'>" +
        "<div class='col'>" +
        "<h6>Pregunta @@: &&</h6>" +
        "</div>" +
        "</div>";
    let opciones =
        "<div class='row'>";
    let opcion =
        "<div class='col-3'>";
    let radio =
        "<label for=''>" +
        "<input type='radio' name='answer##' value='&&'>" +
        "@@" +
        "</label>";

    pregunta = pregunta.replace("@@", n).replace("&&", content.PREGUNTA);
    opciones += opcion + radio.replace("@@", content.OPCION1).replace("##", n).replace("&&", 1) + "</div>";
    opciones += opcion + radio.replace("@@", content.OPCION2).replace("##", n).replace("&&", 2) + "</div>";
    opciones += opcion + radio.replace("@@", content.OPCION3).replace("##", n).replace("&&", 3) + "</div>";
    opciones += opcion + radio.replace("@@", content.OPCION4).replace("##", n).replace("&&", 4) + "</div>";
    opciones += "</div>";

    return div.replace("@@", pregunta + opciones);

}

function getAutoevaluacion(){
    alert("Mi autevaluacion es 5: Mi compromiso hacia la materia es absoluto, he dedicado el tiempo y esfuerzo durante el aprendizaje de cada tema visto en el curso, apesar de los inconvenientes en algunas ocasiones yo sigo aprendiento conforme avanzamos.");
}